var https = require('https');
var fs = require("fs");

for(let index = 0; index < 104; index ++){
    var url = `https://res.wx.qq.com/mpres/htmledition/images/icon/emotion/${index}.gif`;
    https.get(url, function (res) {
        var imgData = "";
        res.setEncoding("binary"); //一定要设置response的编码为binary否则会下载下来的图片打不开
        res.on("data", function (chunk) {
            imgData += chunk;
        });
        res.on("end", function () {
            fs.writeFile(`./img/${index}.gif`, imgData, "binary", function (err) {
                if (err) {
                    console.log("保存失败");
                }
                console.log(`${index}.gif保存成功`);
            });
        });
    });
}